import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Interactive(),
    );
  }
}

class Interactive extends StatefulWidget {
  @override
  _InteractiveState createState() => _InteractiveState();
}

class _InteractiveState extends State<Interactive> {
  Color bgColor = Colors.white;
  TextEditingController controller = new TextEditingController();
  String text = 'a';

  @override
  void initState() {
    super.initState();
    controller.text = text;
  }

  @override
  Widget build(BuildContext context) {
    print(bgColor);
    return Scaffold(
      // floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      appBar: AppBar(
        title: Text('Interactive'),
      ),
      body: Container(
        color: bgColor,
        width: double.infinity,
        height: double.infinity,
        child: Column(
          children: [
            FlatButton(
              color: Colors.red,
              onPressed: () {
                bgColor = Colors.red;
                setState(() {});
              },
              child: Text('Click me to change color to red'),
            ),
            Container(
              height: 20,
            ),
            RaisedButton(
              onPressed: () {
                setState(() {
                  bgColor = Colors.pink;
                });
                controller.clear();
              },
              child: Text('Click me to change color to pink'),
              color: Colors.pink,
            ),
            Container(
              height: 20,
            ),
            IconButton(
              icon: Icon(Icons.emoji_emotions_outlined),
              color: Colors.yellow,
              onPressed: () {
                setState(() {
                  bgColor = Colors.yellow;
                });
              },
            ),
            Container(
              height: 20,
            ),
            FlatButton.icon(
              icon: Icon(
                Icons.emoji_flags,
                color: Colors.white,
              ),
              color: Colors.green,
              label: Text('Click me to change color to green'),
              onPressed: () {
                setState(() {
                  bgColor = Colors.green;
                });
              },
            ),
            Container(
              height: 20,
            ),
            FlatButton(
              color: Colors.black,
              onPressed: () {
                setState(() {
                  bgColor = Colors.black;
                });
              },
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  children: [
                    Icon(
                      Icons.emoji_symbols,
                      color: Colors.white,
                    ),
                    Text(
                      'Click me to change color to red',
                      style: TextStyle(color: Colors.white),
                    ),
                  ],
                ),
              ),
            ),
            Container(
              height: 100,
            ),
            // user input section
            Text(
              text,
              style: TextStyle(fontSize: 50),
            ),
            SizedBox(
              width: 400,
              child: TextField(
                decoration: InputDecoration(
                  icon: Icon(Icons.add),
                  labelText: 'Mobile number',
                  hintText: 'Mobile Number',
                  errorText: 'Too short',
                  border: OutlineInputBorder(),
                  isDense: true,
                ),
                keyboardType: TextInputType.number,
                autofocus: false,
                // obscureText: true,
                // obscuringCharacter: '#',
                onChanged: (value) {
                  print(value);
                },
                controller: controller,
              ),
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {},
        child: Text('ADddd'),
      ),
    );
  }
}
